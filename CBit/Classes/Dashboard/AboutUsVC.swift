import UIKit

class AboutUsVC: UIViewController {
    
    //MARK: - Properties
    @IBOutlet weak var webViewAboutUs: UIWebView!
    @IBOutlet weak var progressAboutUs: UIProgressView!
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        webViewAboutUs.delegate = self
        setAboutUs()
    }
    
    func setAboutUs() {
        webViewAboutUs.loadRequest(URLRequest(url: URL(string: "http://cbitoriginal.com")!))
    }
    
    //MARK: - Button Method
    @IBAction func buttonMenu(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

//MARK: - WebView Delegate Method
extension AboutUsVC: UIWebViewDelegate {
    func webViewDidStartLoad(_ webView: UIWebView) {
        progressAboutUs.setProgress(0.1, animated: false)
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        progressAboutUs.setProgress(1.0, animated: true)
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        progressAboutUs.setProgress(1.0, animated: true)
    }
}
