import UIKit

class FAQsVC: UIViewController {

    
   
        @IBOutlet weak var webViewTermsAndCondition: UIWebView!
        @IBOutlet weak var progressTermsAndCondition: UIProgressView!
        
        override func viewDidLoad() {
           super.viewDidLoad()
            
                    webViewTermsAndCondition.delegate = self
                    webViewTermsAndCondition.loadRequest(URLRequest(url: URL(string: Define.FAQsURL)!))
                    
                }
                
                //MARK: - Button Method
                @IBAction func buttonMenu(_ sender: Any) {
                    self.dismiss(animated: true, completion: nil)
                }
        @IBAction func btn_back(_ sender: Any) {
            
            self.dismiss(animated: true)
        }
    }

            //MARK: - WebView Delegate Method
            extension FAQsVC: UIWebViewDelegate {
                func webViewDidStartLoad(_ webView: UIWebView) {
                    progressTermsAndCondition.setProgress(0.1, animated: false)
                }
                func webViewDidFinishLoad(_ webView: UIWebView) {
                    progressTermsAndCondition.setProgress(1.0, animated: true)
                }
                func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
                    progressTermsAndCondition.setProgress(1.0, animated: true)
                }

    
}
