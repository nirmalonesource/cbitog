import UIKit
import AVFoundation

class GamePlayVC: UIViewController {
    
    
    @IBOutlet weak var btnlock: UIImageView!
    
    @IBOutlet weak var imglock09: UIImageView!
    @IBOutlet weak var btnzero: UIButton!
    
    @IBOutlet weak var btnonee: UIButton!
    
    @IBOutlet weak var btntwo: UIButton!
    
    @IBOutlet weak var btnthree: UIButton!
    
    @IBOutlet weak var btnfour: UIButton!
    
    @IBOutlet weak var btnfive: UIButton!
    
    @IBOutlet weak var btnsix: UIButton!
    
    
    @IBOutlet weak var btnseven: UIButton!
    
    @IBOutlet weak var btneeight: UIButton!
    
    
    @IBOutlet weak var btnnine: UIButton!
    
    @IBOutlet weak var buttonAnsMinus: ButtonWithRadius!
    
    @IBOutlet weak var labelanswerselected: UILabel!
    
    @IBOutlet weak var buttonAnsZero: ButtonWithRadius!
    
    
    @IBOutlet weak var buttonAnsPlus: ButtonWithRadius!
    
    @IBOutlet weak var viewrdb: UIView!
    
    @IBOutlet weak var view0to9: UIView!
    
    @IBOutlet weak var btnlockall: ButtonWithRadius!
    
    
    var lockall :String = ""
    var gametype : String = ""
    var strDisplayValuelockall: String?
    
    @IBOutlet weak var labelAnsMinus: UILabel!
    @IBOutlet weak var labelAnsZero: UILabel!
    
    @IBOutlet weak var labelAnsPlus: UILabel!
    
    //MARK: - Properites
    @IBOutlet weak var viewTimmer: UIView!
    @IBOutlet weak var collectionGame: UICollectionView!
    @IBOutlet weak var tableAnswer: UITableView!
    @IBOutlet weak var labelContestName: UILabel!
    @IBOutlet weak var labelTimer: UILabel!
    @IBOutlet var btnlockallnumber: ButtonWithRadius!
    
    @IBOutlet weak var constrainCollectionViewHeight: NSLayoutConstraint!
    
    var dictContest = [String: Any]()
    var isFromNotification = Bool()
    
    var dictGameData = [String: Any]()
    private var arrTickets = [[String: Any]]()
    private var arrSelectedTicket = [[String: Any]]()
    private var arrRandomNumbers = [Int]()
    private var arrBrackets = [[String: Any]]()
    private var currentDate = Date()
    var gamelevel = Int()
    
    private var isGameStart = Bool()
    private var isStartEventCall = Bool()
    
    var timer: Timer?
    var second = Int()
    var msecond:Int = 999
    
    var endGameTimer: Timer?
    var endGameSecond = 20
    
    var startTimer: Timer?
    var startSecond = Int()
    var miliSecondValue = 0
    var differenceSecond = Int()
    

    var arrBarcketColor = [BracketData]()
    
    private var indexOfElement = Int()
    private var indexOfPath = 2
    var gameMode = String()
    
    var isShowLoading = Bool()
    //Sound
    var setSoundEffect: AVAudioPlayer?
    var soundURL: URL?
    
    var viewAnimation: ViewAnimation?
    var Lockall1: LockAll?
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        tableAnswer.rowHeight = UITableView.automaticDimension
        tableAnswer.tableFooterView = UIView()
        
        if !isFromNotification {
            
            gamelevel = dictContest["level"] as? Int ?? 1
            let date = dictContest["startDate"] as! String
            let startDate = MyModel().converStringToDate(strDate: date, getFormate: "yyyy-MM-dd HH:mm:ss")
            let calender = Calendar.current
            let unitFlags = Set<Calendar.Component>([ .second])
            let dateComponent = calender.dateComponents(unitFlags, from: Date(), to: startDate)
            
            if dateComponent.second! < 0 {
                
                startSecond = 0
                
            } else {
                
                startSecond = dateComponent.second!
                
            }
            
            //setData()
        }
        
        //print(arrBarcketColor.count)
        
        //Add Notificaton
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleNotification(_:)),
                                               name: .startGame,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleEndGameNotication(_:)),
                                               name: .endGame,
                                               object: nil)
        let path = Bundle.main.path(forResource: "Tick Tock.mp3", ofType: nil)!
        soundURL = URL(fileURLWithPath: path)
        isShowLoading = true
        getContestDetail()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isFromNotification {
            labelTimer.text = "Game starts in \(timeString(time: TimeInterval(startSecond)))"
        }
        SocketIOManager.sharedInstance.lastViewController = self
        SwiftPingPong.shared.startPingPong()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SocketIOManager.sharedInstance.lastViewController = nil
        SwiftPingPong.shared.stopPingPong()
    }
    
    
    @IBAction func btn_nopressed(_ sender: UIButton) {
        if sender.tag == 0
        {
            strDisplayValuelockall = "0"
             btnzero.backgroundColor = UIColor.white
             btnonee.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
             btntwo.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
             btnthree.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
             btnfour.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
             btnfive.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
             btnsix.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
             btnseven.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
             btneeight.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
             btnnine.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            
        }
        else if sender.tag == 1
        {
            
            strDisplayValuelockall = "1"
          
            btnzero.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnonee.backgroundColor = UIColor.white
            btntwo.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnthree.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnfour.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnfive.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnsix.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnseven.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btneeight.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnnine.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
        
        }
        else if sender.tag == 2
        {
            strDisplayValuelockall = "2"
        
            btnzero.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnonee.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btntwo.backgroundColor = UIColor.white
            btnthree.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnfour.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnfive.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnsix.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnseven.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btneeight.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnnine.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
        }
        else if sender.tag == 3
        {
            strDisplayValuelockall = "3"
            btnzero.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnonee.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btntwo.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnthree.backgroundColor = UIColor.white
            btnfour.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnfive.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnsix.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnseven.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btneeight.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnnine.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            
        }else if sender.tag == 4
        {
            strDisplayValuelockall = "4"
            
            btnzero.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnonee.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btntwo.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnthree.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnfour.backgroundColor = UIColor.white
            btnfive.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnsix.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnseven.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btneeight.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnnine.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)

        }
        else if sender.tag == 5
        {
            strDisplayValuelockall = "5"
            btnzero.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnonee.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btntwo.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnthree.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnfour.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnfive.backgroundColor = UIColor.white
            btnsix.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnseven.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btneeight.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
               btnnine.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
        }
        else if sender.tag == 6
        {
            strDisplayValuelockall = "6"
            btnzero.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnonee.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btntwo.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnthree.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnfour.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnfive.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnsix.backgroundColor = UIColor.white
            btnseven.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btneeight.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnnine.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
        }
        else if sender.tag == 7
        {
            strDisplayValuelockall = "7"
            
            btnzero.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnonee.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btntwo.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnthree.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnfour.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnfive.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnsix.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnseven.backgroundColor = UIColor.white
            btnseven.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btneeight.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnnine.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
        }
        else if sender.tag == 8
        {
            strDisplayValuelockall = "8"
            btnzero.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnonee.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btntwo.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnthree.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnfour.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnfive.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnsix.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnseven.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btneeight.backgroundColor = UIColor.white
            btnnine.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
        }
        else if sender.tag == 9
        {
            strDisplayValuelockall = "9"
            btnzero.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnonee.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btntwo.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnthree.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnfour.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnfive.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnsix.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnseven.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btneeight.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
            btnnine.backgroundColor = UIColor.white
            
        }
         
    }
    
    @IBAction func btn_lockall(_ sender: Any) {
        
//         let fixCell = tableAnswer.dequeueReusableCell(withIdentifier: "GameAnswerTwoTVC") as! GameAnswerTwoTVC
//        fixCell.buttonLockNow.isEnabled = false
//        fixCell.buttonLockNow.alpha = 0.5
//        if LockAll == nil {
//            LockAll = LockAll.LockAll.instanceFromNib() as? LockAll
//            LockAll!.frame = view.bounds
//            view.addSubview(LockAll!)
//        }
        
        print("selection",strDisplayValuelockall)
        if strDisplayValuelockall == nil
        {
           
        }
        else{
            locakall()
        }
        
    }
    
    func locakall()
    {
        if Lockall1 == nil {
               Lockall1 = LockAll.instanceFromNib() as? LockAll
               Lockall1!.frame = view.bounds
               view.addSubview(Lockall1!)

           }
           
        
           
          
           btnlockall.isEnabled = false
           btnlockall.alpha = 0.5
           
           btnlock.isHidden = false
           imglock09.isHidden = false
           
           btnlockallnumber.isEnabled = false
           btnlockallnumber.alpha = 0.5
           
           buttonAnsMinus.isEnabled = false
           buttonAnsPlus.isEnabled = false
           buttonAnsZero.isEnabled = false
           btnonee.isEnabled = false
           btntwo.isEnabled = false
           btnthree.isEnabled = false
           btnfour.isEnabled = false
           btnfive.isEnabled = false
           btnsix.isEnabled = false
           btnseven.isEnabled = false
           btneeight.isEnabled = false
           btnnine.isEnabled = false
           btnzero.isEnabled = false

           
           
          
               let parameter:[String: Any] = ["userId": Define.USERDEFAULT.value(forKey: "UserID")!,
                                              "contestId": dictContest["id"]!,
                                              "DisplayValue":strDisplayValuelockall ?? ""
                                              
               ]
               print("Parameter: \(parameter)")
               
               let jsonString = MyModel().getJSONString(object: parameter)
               let encriptString = MyModel().encrypting(strData: jsonString!, strKey: Define.KEY)
               let strBase64 = encriptString.toBase64()
             //  cell.buttonLockNow.isEnabled = false
               SocketIOManager.sharedInstance.socket.emitWithAck("updateGameAll", strBase64!).timingOut(after: 0) { (data) in
                   print("Data: \(data)")
                   
                   guard let strValue = data[0] as? String else {
                       Alert().showAlert(title: "Alert",
                                         message: Define.ERROR_SERVER,
                                         viewController: self)
                       return
                   }
                   
                   
                   let strJSON = MyModel().decrypting(strData: strValue, strKey: Define.KEY)
                   let dictData = MyModel().convertToDictionary(text: strJSON)
                   print("Get Data: \(dictData!)")
                   
                   let status = dictData!["statusCode"] as? Int ?? 0
                   if status == 200 {
                       self.Lockall1?.removeFromSuperview()
                       
                       var dictItemData1 = dictData!["content"] as! [[String: Any]]
                       print(dictItemData1)
                       
                       var i = 0
                       
                       for i in i..<self.arrSelectedTicket.count {
                           
                       var dictItemData = dictItemData1[i]
                       var dictTicket = self.arrSelectedTicket[i]
                       dictTicket["isLock"] = dictItemData["isLockAll"]
                      
                       dictTicket["lockTime"] = dictItemData["isLockTime"]
                       self.arrSelectedTicket[i] = dictTicket
                       let indexPath = IndexPath(row:i, section: 0)
                       self.tableAnswer.reloadRows(at: [indexPath], with: .none)
                       }
                   }
               }
        
    }
    
    @IBAction func btnAnsMinus(_ sender: Any) {
        
         let arrSloats = arrSelectedTicket[0]["slotes"] as! [[String: Any]]
        
      // labelanswerselected.text! = arrSloats[0]["displayValue"] as? String ?? "-"
        labelanswerselected.text! = labelAnsMinus.text ?? ""
        buttonAnsMinus.backgroundColor = UIColor.red
          buttonAnsMinus.backgroundColor = UIColor.white
         // buttonAnsPlus.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
        //  buttonAnsZero.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
        labelAnsMinus.textColor = UIColor.black
        labelAnsPlus.textColor = UIColor.white
        
        buttonAnsPlus.backgroundColor = #colorLiteral(red: 0.01568627451, green: 0.2, blue: 1, alpha: 1)
        buttonAnsZero.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
        
          strDisplayValuelockall = arrSloats[0]["displayValue"] as? String ?? ""
        
        
    }
    
    
    @IBAction func btnAnsZero(_ sender: Any) {
        
        
          let arrSloats = arrSelectedTicket[0]["slotes"] as! [[String: Any]]
      //  labelanswerselected.text! = arrSloats[1]["displayValue"] as? String ?? "0"
        
        labelanswerselected.text! = labelAnsZero.text ?? ""
        
        buttonAnsPlus.backgroundColor = #colorLiteral(red: 0.01568627451, green: 0.2, blue: 1, alpha: 1)
        buttonAnsMinus.backgroundColor = UIColor.red
        buttonAnsZero.backgroundColor = UIColor.white
        
        labelAnsPlus.textColor = UIColor.white
        labelAnsMinus.textColor = UIColor.white
        
       //  buttonAnsPlus.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
        //buttonAnsMinus.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
         strDisplayValuelockall = arrSloats[1]["displayValue"] as? String ?? ""
        
    }
    
    @IBAction func btnAnsPlua(_ sender: Any) {
          let arrSloats = arrSelectedTicket[0]["slotes"] as! [[String: Any]]
        // labelanswerselected.text = arrSloats[2]["displayValue"] as? String ?? "+"
          labelanswerselected.text! = labelAnsPlus.text ?? ""
          buttonAnsPlus.backgroundColor = UIColor.white
          labelAnsPlus.textColor = UIColor.black
          labelAnsMinus.textColor = UIColor.white
          buttonAnsMinus.backgroundColor = UIColor.red
          buttonAnsZero.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
          
        
       //  buttonAnsZero.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
       //  buttonAnsMinus.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
         strDisplayValuelockall = arrSloats[2]["displayValue"] as? String ?? ""
    }
    
    override func viewWillLayoutSubviews() {
        viewTimmer.layer.cornerRadius = 15
        viewTimmer.layer.masksToBounds = true
        
        labelContestName.text = dictContest["name"] as? String ?? "No Name"
        
    }
    
    
    func getContestDetail() {
        if isShowLoading {
            Loading().showLoading(viewController: self)
        }
        let parameter:[String: Any] = [ "contestId": dictContest["id"]!, "userId":Define.USERDEFAULT.value(forKey: "UserID")!]
        print("Parameter: \(parameter)")
        
        let jsonString = MyModel().getJSONString(object: parameter)
        let encriptString = MyModel().encrypting(strData: jsonString!, strKey: Define.KEY)
        let strBase64 = encriptString.toBase64()
        let sendRequestTime = Date()
        SocketIOManager.sharedInstance.socket.emitWithAck("contestDetails", strBase64!).timingOut(after: 0) {data in
            print("Game Data: \(data)")
            guard let strValue = data[0] as? String else {
                Alert().showAlert(title: "Alert",
                                  message: Define.ERROR_SERVER,
                                  viewController: self)
                return
            }
            let getResponceTime = Date()
            
            let calender = Calendar.current
            let unitFlags = Set<Calendar.Component>([ .second])
            let dateComponent = calender.dateComponents(unitFlags, from: sendRequestTime, to: getResponceTime)
            
            self.differenceSecond = dateComponent.second!
            
            print("=> The Difference Of Second is: ", self.differenceSecond)
            let strJSON = MyModel().decrypting(strData: strValue, strKey: Define.KEY)
            let dictData = MyModel().convertToDictionary(text: strJSON)
            print(dictData)
            print(self.dictContest)
            self.dictGameData = dictData!["content"] as! [String: Any]
            print(self.dictGameData)
            
            self.gametype  = self.dictContest["game_type"] as! String
            if self.gametype == "rdb" {
                self.viewrdb.isHidden = false
                self.buttonAnsMinus.backgroundColor = UIColor.red
                
            }
            else
            {
                self.view0to9.isHidden = false
            }
            let serverDate = self.dictGameData["currentTime"] as? String ?? "\(MyModel().convertDateToString(date: Date(), returnFormate: "yyyy-MM-dd HH:mm:ss"))"
            self.currentDate = MyModel().converStringToDate(strDate: serverDate, getFormate: "yyyy-MM-dd HH:mm:ss")
            
            print("➤ \(self.dictGameData)")
            self.setData()
            Loading().hideLoading(viewController: self)
        }
    }
    
    func   setData() {
        
        arrTickets = dictGameData["tickets"] as! [[String: Any]]
        arrBrackets = dictGameData["boxJson"] as! [[String: Any]]
        gameMode = dictGameData["gameMode"] as? String ?? "public"
        if isFromNotification {
            dictContest["id"] = dictGameData["id"]!
            dictContest["level"] = dictGameData["level"]!
            dictContest["name"] = dictGameData["name"]!
            dictContest["rows"] = dictGameData["rows"]!
            dictContest["startDate"] = dictGameData["startDate"]!
            dictContest["type"] = dictGameData["type"]!
            
            viewWillAppear(true)
            gamelevel = dictContest["level"] as? Int ?? 1
        }
        
        if arrBarcketColor.count <= 0 {
            SetRandomNumber()
        }
        
        arrSelectedTicket.removeAll()
        for item in arrTickets {
            let isPurchased = item["isPurchased"] as? Bool ?? false
            let gameMode = dictContest["type"] as? Int ?? 0
            var dictData = item
            if gameMode == 0 {
                dictData["selectedValue"] = dictGameData["ansRangeMin"] as? Float ?? 0.0
            }
            
            if isPurchased {
                arrSelectedTicket.append(dictData)
            }
        }
        
        //notStart
        //start
    //gameEnd
        let gameStatus = dictGameData["gameStatus"] as? String ?? "notStart"
        if gameStatus == "start" {
            isGameStart = true
            
            print(dictGameData["duration"] as? Int)
            second = ((dictGameData["duration"] as? Int ?? 30) - differenceSecond)
            self.labelTimer.text = "\(self.second)"
            second = second - 1
            setTimer()
        } else if gameStatus == "gameEnd" {
            
            NotificationCenter.default.removeObserver(self)
            SocketIOManager.sharedInstance.lastViewController = nil
            let resultVC = self.storyboard?.instantiateViewController(withIdentifier: "GameResultVC") as! GameResultVC
            resultVC.dictContest = dictContest
            self.navigationController?.pushViewController(resultVC, animated: true)
        } else {
            isGameStart = false
            
            let date = dictGameData["startDate"] as! String
            let startDate = MyModel().converStringToDate(strDate: date, getFormate: "yyyy-MM-dd HH:mm:ss")
            
            let calender = Calendar.current
            let unitFlags = Set<Calendar.Component>([ .second])
            let dateComponent = calender.dateComponents(unitFlags, from: self.currentDate, to: startDate)
            startSecond = dateComponent.second! - differenceSecond
            //startSecond = MyModel().getSecound(currentTime: self.currentDate, startDate: startDate)
            print("Seconds: \(startSecond)")
            
            if startTimer == nil {
                startSecond = startSecond - 1
                labelTimer.text = "Game starts in \(timeString(time: TimeInterval(startSecond)))"
                setStartTimer()
            }
        }
        collectionGame.reloadData()
        tableAnswer.reloadData()
    }
    
    func SetRandomNumber() {
        self.view.layoutIfNeeded()
        let rangeMinNumber = dictGameData["ansRangeMin"] as? Int ?? 0
        let rangeMaxNumber = dictGameData["ansRangeMax"] as? Int ?? 99
        if gamelevel == 1 {
            arrRandomNumbers = MyModel().createRandomNumbers(number: 8, minRange: rangeMinNumber, maxRange: rangeMaxNumber)
            arrBarcketColor = MyDataType().getArrayBrackets(index: 8)
            constrainCollectionViewHeight.constant = 50
        } else if gamelevel == 2 {
            arrRandomNumbers = MyModel().createRandomNumbers(number: 16, minRange: rangeMinNumber, maxRange: rangeMaxNumber)
            arrBarcketColor = MyDataType().getArrayBrackets(index: 16)
            constrainCollectionViewHeight.constant = 100
        } else if gamelevel == 3 {
            arrRandomNumbers = MyModel().createRandomNumbers(number: 32, minRange: rangeMinNumber, maxRange: rangeMaxNumber)
            arrBarcketColor = MyDataType().getArrayBrackets(index: 32)
            constrainCollectionViewHeight.constant = 200
        }
        self.view.layoutIfNeeded()
    }
    
    func updateColors() {
        for _ in 1...4 {
            let index = arrBarcketColor.count
            let lastColor = arrBarcketColor[index - 1]
            arrBarcketColor.remove(at: arrBarcketColor.count - 1)
            arrBarcketColor.insert(lastColor, at: 0)
        }
        
        let rangeMinNumber = dictGameData["ansRangeMin"] as? Int ?? 0
        let rangeMaxNumber = dictGameData["ansRangeMax"] as? Int ?? 99
        
        if gamelevel == 1 {
            arrRandomNumbers = MyModel().createRandomNumbers(number: 8, minRange: rangeMinNumber, maxRange: rangeMaxNumber)
        } else if gamelevel == 2 {
            arrRandomNumbers = MyModel().createRandomNumbers(number: 16, minRange: rangeMinNumber, maxRange: rangeMaxNumber)
        } else if gamelevel == 3 {
            arrRandomNumbers = MyModel().createRandomNumbers(number: 32, minRange: rangeMinNumber, maxRange: rangeMaxNumber)
        }
    }
    
    @objc func handleNotification(_ notification: Notification) {
        print("Game Start.")
        isStartEventCall = true
        if (notification.userInfo as Dictionary?) != nil {
            print("--> User Info Data: \(notification.userInfo!)")
            let contestId = "\(notification.userInfo!["contestId"]!)"
            let selectedContestID = "\(dictContest["id"]!)"
            if contestId == selectedContestID {
                isShowLoading = false
                getContestDetail()
            }
        } else {
            print("--> No Data")
            isShowLoading = false
            getContestDetail()
        }
    }
    
    @objc func handleEndGameNotication(_ notification: Notification) {
        print("Gaem End")
        
        if (notification.userInfo as Dictionary?) != nil {
            print("--> User Info Data: \(notification.userInfo!)")
            let contestId = "\(notification.userInfo!["contestId"]!)"
            let selectedContestID = "\(dictContest["id"]!)"
            if contestId == selectedContestID {
                setEndTimer()
            }
        } else {
            print("--> No Data")
            setEndTimer()
        }
        
    }
    
    func setColors() {
        
    }
    
    //TODO: Timer
    private func setStartTimer() {
        startTimer = nil
        startTimer = Timer.scheduledTimer(timeInterval: 0.5,
                                     target: self,
                                     selector: #selector(handleStartTimer),
                                     userInfo: nil,
                                     repeats: true)
    }
    
    @objc func handleStartTimer() {
        if miliSecondValue == 0 {
            miliSecondValue = miliSecondValue + 1
            updateColors()
            collectionGame.reloadData()
        } else if miliSecondValue == 1 {
            miliSecondValue = 0
            if startSecond > 1 {
                startSecond = startSecond - 1
                labelTimer.text = "Game starts in \(timeString(time: TimeInterval(startSecond)))"
                updateColors()
                collectionGame.reloadData()
            } else {
                if startTimer != nil {
                    startTimer!.invalidate()
                    startTimer = nil
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                    if !self.isStartEventCall {
                        print("If Start Event call.")
                        self.isStartEventCall = true
                        self.isShowLoading = false
                        self.getContestDetail()
                    }
                }
            }
        }
    }
    
    func timeString(time: TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let secounds = Int(time) % 60
        
        let strTime = String(format: "%02i:%02i:%02i", hours, minutes, secounds)
        return strTime
    }
    
    private func setTimer() {
        if startTimer != nil {
            startTimer!.invalidate()
            startTimer = nil
        }
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval:1,
                                         target: self,
                                         selector: #selector(handleTimer),
                                         userInfo: nil,
                                         repeats: true)
           
        }
    }
    
    @objc func handleTimer(){
        if second > 0 {
           // print("Second: \(second)")
            
//            if msecond > 0 {
//                print("\(second-1)" + ":"  + "\(msecond)")
//
//                msecond = msecond - 1
//                self.labelTimer.text = "\(second-1)" + ":"  + "\(msecond)"
//            }
            
            self.labelTimer.text = String(format: "%02i", self.second)
            //print(labelTimer.text!)
            second = second - 1
            msecond = 999
            
        } else {
            //print("Timer Not Start")
            if timer != nil {
                self.labelTimer.text = "00"
                timer!.invalidate()
                timer = nil
            }
        }
    }
    
    private func setEndTimer() {
        
        if endGameTimer == nil {
            
            if viewAnimation == nil {
                viewAnimation = ViewAnimation.instanceFromNib() as? ViewAnimation
                viewAnimation!.frame = view.bounds
                view.addSubview(viewAnimation!)
            }
            
            endGameTimer = Timer.scheduledTimer(timeInterval: 1,
                                                target: self,
                                                selector: #selector(handleEndTimer),
                                                userInfo: nil,
                                                repeats: true)
        }
    }
    
    @objc func handleEndTimer () {
        if endGameSecond > 0 {
            endGameSecond = endGameSecond - 1
            if setSoundEffect == nil {
                setSound()
            }
        } else {
            
            if endGameTimer != nil {
                endGameTimer!.invalidate()
                endGameTimer = nil
                
                if setSoundEffect != nil {
                    setSoundEffect!.stop()
                }
                
                NotificationCenter.default.removeObserver(self)
                let resultVC = self.storyboard?.instantiateViewController(withIdentifier: "GameResultVC") as! GameResultVC
                resultVC.dictContest = dictContest
                self.navigationController?.pushViewController(resultVC, animated: true)
            }
        }
    }
    
    func setSound() {
        do{
            setSoundEffect = try AVAudioPlayer(contentsOf: soundURL!)
            setSoundEffect!.numberOfLoops = 4
            setSoundEffect!.play()
        } catch {
            print("Error In Sound PLay")
        }
    }
    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: Any) {
        NotificationCenter.default.removeObserver(self)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonInfo(_ sender: Any) {
        let gameInfo = GamePlayInfo.instanceFromNib() as! GamePlayInfo
        gameInfo.frame = view.bounds
        view.addSubview(gameInfo)
    }
}

//MARK: - Collection View Delegate Method
extension GamePlayVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isGameStart {
            return arrBrackets.count
        } else {
            return arrRandomNumbers.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width / 4, height: 25)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BracketCVC", for: indexPath) as! BracketCVC
        
        if isGameStart {
            let index = arrBarcketColor[indexPath.row].index
            
            cell.labelNumber.text = "\(arrBrackets[index]["number"]!)"
            cell.viewColor.backgroundColor = arrBarcketColor[indexPath.row].color

            //            let strColor = arrBrackets[indexPath.row]["color"] as? String ?? "red"
//            if strColor == "red" {
//                cell.viewColor.backgroundColor = UIColor.red
//            } else if strColor == "green" {
//                cell.viewColor.backgroundColor = UIColor.green
//            } else if strColor == "blue" {
//                cell.viewColor.backgroundColor = UIColor.blue
//            }
            
        } else {
            cell.labelNumber.text = "\(arrRandomNumbers[indexPath.row])"
            
            cell.viewColor.backgroundColor = arrBarcketColor[indexPath.row].color
        }
        return cell
    }
}

//MARK: - TableView Delegate
extension GamePlayVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSelectedTicket.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let gameMode = dictContest["type"] as? Int ?? 0
        if gameMode == 0 {
            //TODO: FLEXI
            let isLock = arrSelectedTicket[indexPath.row]["isLock"] as? Bool ?? false
            if isLock {
                let flexiCell = tableView.dequeueReusableCell(withIdentifier: "GameAnsRangeLockTVC") as! GameAnsRangeLockTVC
                
                let strAmount = "\(arrSelectedTicket[indexPath.row]["amount"]!)"
                //flexiCell.labelEntryFees.text = "₹\(MyModel().getNumbers(value: Double(strAmount)!))"
                let test = Double(strAmount) ?? 0.00
                flexiCell.labelEntryFees.text = String(format: "₹ %.02f", test)
                
                let strTickets = "\(arrSelectedTicket[indexPath.row]["totalTickets"]!)"
                
                flexiCell.labelTotalTickets.text = "\(MyModel().getNumbers(value: Double(strTickets)!))"
                let strWinnings = "\(arrSelectedTicket[indexPath.row]["totalWinnings"]!)"
                
                flexiCell.labelTotalWinnig.text = "\(MyModel().getCurrncy(value: Double(strWinnings)!))"
                
                let strWinners = "\(arrSelectedTicket[indexPath.row]["maxWinners"]!)"
                
                flexiCell.labelMaxWinners.text = "\(MyModel().getNumbers(value: Double(strWinners)!))"
                
                flexiCell.labelMinValue.text = "\(dictGameData["ansRangeMin"]!)"
                
                flexiCell.labelMaxValue.text = "\(dictGameData["ansRangeMax"]!)"
                
                flexiCell.sliderAnswer.minimumValue = dictGameData["ansRangeMin"] as? Float ?? 0.0
                flexiCell.sliderAnswer.maximumValue = dictGameData["ansRangeMax"] as? Float ?? 0.0
                
                let rangeMin = dictGameData["ansRangeMin"] as? Float ?? 0.0
                let rangeMax = dictGameData["ansRangeMax"] as? Float ?? 0.0
                
                let range = arrSelectedTicket[indexPath.row]["bracketSize"] as? Float ?? 0.0
                flexiCell.sliderAnswer.setThumbImage(MyModel().getImageForRange(range: Int(range),
                                                                                rangeMaxValue: Int(abs(rangeMin - rangeMax))),
                                                     for: .normal)
                
                flexiCell.sliderAnswer.isUserInteractionEnabled = false
                
                //let strSelected = arrSelectedTicket[indexPath.row]["displayValue"] as? String ?? "0 to 0"
                
                let dictUserSelectData = arrSelectedTicket[indexPath.row]["user_select"] as! [String: Any]
                print("Selected Data: \(dictUserSelectData)")
                
                let strSelectedValueMin = "\(dictUserSelectData["startValue"]!)"
                let selectedRangeMin = Int(strSelectedValueMin)
                let strSelectedValueMax = "\(dictUserSelectData["endValue"]!)"
                let selectedRangeMax = Int(strSelectedValueMax)
                
                print("Start Value: \(selectedRangeMin!), End Value: \(selectedRangeMax!)")
                
                flexiCell.labelAnsSelected.text = "\(dictUserSelectData["startValue"]!) to \(dictUserSelectData["endValue"]!)"
                if selectedRangeMax == Int(rangeMax) {
                    flexiCell.sliderAnswer.value = rangeMax
                } else if (selectedRangeMin! + Int(range)) > Int(rangeMax) {
                    flexiCell.sliderAnswer.value = Float(selectedRangeMin!)
                } else if (selectedRangeMax! - Int(range)) < Int(rangeMin) {
                    flexiCell.sliderAnswer.value = Float(selectedRangeMax!)
                } else {
                    flexiCell.sliderAnswer.value = Float(selectedRangeMin!)
                }
                
                flexiCell.labelLockTime.text = "locked at: \(arrSelectedTicket[indexPath.row]["lockTime"] as? String ?? "--")"
                
                return flexiCell
            } else {
                let flexiCell = tableView.dequeueReusableCell(withIdentifier: "GameAnsRangeTVC") as! GameAnsRangeTVC
                
                let dictUserSelectData = arrSelectedTicket[indexPath.row]["user_select"] as! [String: Any]
                print("Selected Data: \(dictUserSelectData)")
                
                if isLock {
                    flexiCell.viewUnLocked.isHidden = true
                    flexiCell.viewLocked.isHidden = false
                    flexiCell.labelLockedAnswer.text = "\(dictUserSelectData["startValue"]!) to \(dictUserSelectData["endValue"]!)"
                    flexiCell.labelLockedAt.text = "locked at: \(arrSelectedTicket[indexPath.row]["lockTime"] as? String ?? "--")"
                } else {
                    flexiCell.viewUnLocked.isHidden = false
                    flexiCell.viewLocked.isHidden = true
                    flexiCell.labelLockedAt.text = ""
                }
                
                flexiCell.delegate = self
                
                let strAmount = "\(arrSelectedTicket[indexPath.row]["amount"]!)"
                //flexiCell.labelEntryFees.text = "₹\(MyModel().getNumbers(value: Double(strAmount)!))"
                let test = Double(strAmount) ?? 0.00
                flexiCell.labelEntryFees.text = String(format: "₹ %.02f", test)
                
                
                
                let strTickets = "\(arrSelectedTicket[indexPath.row]["totalTickets"]!)"
                flexiCell.labelTotalTickets.text = "\(MyModel().getNumbers(value: Double(strTickets)!))"
                let strWinnings = "\(arrSelectedTicket[indexPath.row]["totalWinnings"]!)"
                flexiCell.labelTotalWinnig.text = "\(MyModel().getCurrncy(value: Double(strWinnings)!))"
                let strWinners = "\(arrSelectedTicket[indexPath.row]["maxWinners"]!)"
                flexiCell.labelMaxWinners.text = "\(MyModel().getNumbers(value: Double(strWinners)!))"
                
                flexiCell.labelMinRange.text = "\(dictGameData["ansRangeMin"]!)"
                flexiCell.labelMaxRange.text = "\(dictGameData["ansRangeMax"]!)"
                
                flexiCell.sliderAnswer.minimumValue = dictGameData["ansRangeMin"] as? Float ?? 0.0
                flexiCell.sliderAnswer.maximumValue = dictGameData["ansRangeMax"] as? Float ?? 0.0
                //flexiCell.sliderAnswer.value = arrSelectedTicket[indexPath.row]["selectedValue"] as? Float ?? 0.0
                flexiCell.selectedIndex = arrSelectedTicket[indexPath.row]["selectedValue"] as? Double ?? 0.0
                
                let rangeMin = dictGameData["ansRangeMin"] as? Float ?? 0.0
                let rangeMax = dictGameData["ansRangeMax"] as? Float ?? 0.0
                
                let range = arrSelectedTicket[indexPath.row]["bracketSize"] as? Float ?? 0.0
                flexiCell.sliderAnswer.setThumbImage(MyModel().getImageForRange(range: Int(range),
                                                                                rangeMaxValue: Int(abs(rangeMin - rangeMax))),
                                                     for: .normal)

                
                flexiCell.dictData = nil
                flexiCell.dictData = arrSelectedTicket[indexPath.row]
                
                flexiCell.buttonLockNow.addTarget(self,
                                                  action: #selector(buttonRangeAnsLock(_:)),
                                                  for: .touchUpInside)
                flexiCell.buttonLockNow.tag = indexPath.row
                
                if isGameStart {
                    flexiCell.sliderAnswer.value = rangeMin
                    flexiCell.buttonLockNow.isEnabled = true
                    flexiCell.buttonLockNow.alpha = 1
                    flexiCell.sliderAnswer.isUserInteractionEnabled = true
                    flexiCell.imageDummyBar.isHidden = true
                    flexiCell.labelDummyBarVal.isHidden = true
                    flexiCell.isGameStart = true
                    flexiCell.labelAnsSelected.text = "\(Int(flexiCell.rangeMin)) to \(Int(flexiCell.rangeMax))"
                } else {
                    flexiCell.sliderAnswer.setThumbImage(UIImage(), for: .normal)
                    flexiCell.sliderAnswer.isUserInteractionEnabled = false
                    flexiCell.buttonLockNow.isEnabled = false
                    flexiCell.buttonLockNow.alpha = 0.5
                    flexiCell.imageDummyBar.isHidden = false
                    flexiCell.labelDummyBarVal.isHidden = false
                    flexiCell.imageDummyBar.image = MyModel().getImageForRange(range: Int(range),
                                                                               rangeMaxValue: Int(abs(rangeMin - rangeMax)))
                    flexiCell.labelDummyBarVal.text = "\(Int(range))"
                    flexiCell.isGameStart = false
                }
                return flexiCell
            }
        } else {
            //TODO: FIX
            let arrSloats = arrSelectedTicket[indexPath.row]["slotes"] as! [[String: Any]]
            
            if arrSloats.count == 3 {
                
                let isLock = arrSelectedTicket[indexPath.row]["isLock"] as? Bool ?? false
                
                if isLock {
                    let fixCell = tableView.dequeueReusableCell(withIdentifier: "GameAnswerOneTVC") as! GameAnswerOneTVC
                    
                    let strAmount = "\(arrSelectedTicket[indexPath.row]["amount"]!)"
                    //fixCell.labelEntryFees.text = "₹\(MyModel().getNumbers(value: Double(strAmount)!))"
                    let test = Double(strAmount) ?? 0.00
                    fixCell.labelEntryFees.text = String(format: "₹ %.02f", test)
                    
                    let strTickets = "\(arrSelectedTicket[indexPath.row]["totalTickets"]!)"
                    fixCell.labelTotalTickets.text = "\(MyModel().getNumbers(value: Double(strTickets)!))"
                    let strWinnings = "\(arrSelectedTicket[indexPath.row]["totalWinnings"]!)"
                    fixCell.labelTotalWinnig.text = "\(MyModel().getCurrncy(value: Double(strWinnings)!))"
                    let strWinners = "\(arrSelectedTicket[indexPath.row]["maxWinners"]!)"
                    fixCell.labelMaxWinners.text = "\(MyModel().getNumbers(value: Double(strWinners)!))"
                    
                    fixCell.labelAnsMinus.text = arrSloats[0]["displayValue"] as? String ?? "-"
                    fixCell.labelAnsZero.text = arrSloats[1]["displayValue"] as? String ?? "0"
                    fixCell.labelAnsPlus.text = arrSloats[2]["displayValue"] as? String ?? "+"
                    
                    let string1 = arrSloats[0]["displayValue"] as? String ?? "-"
                                   let string2 = arrSloats[2]["displayValue"] as? String ?? "-"
                                   
                                   if string1.localizedCaseInsensitiveContains("red win")
                                   {
                                     fixCell.vwWithradius.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.8862745098, blue: 0.8862745098, alpha: 1)
                                      // fixCell.viewRange.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.8862745098, blue: 0.8862745098, alpha: 1)
                                       fixCell.buttonAnsMinus.backgroundColor = UIColor.red
                                       fixCell.labelAnsMinus.textColor = UIColor.white
                                       
                                   }
                                   if string2.localizedCaseInsensitiveContains("blue win")
                                   {
                                       fixCell.buttonAnsPlus.backgroundColor = #colorLiteral(red: 0.01085097995, green: 0.3226040006, blue: 1, alpha: 1)
                                       fixCell.labelAnsPlus.textColor = UIColor.white
                                   }
                    
//                  labelAnsMinus.text = arrSloats[0]["displayValue"] as? String ?? "-"
//                  labelAnsZero.text = arrSloats[1]["displayValue"] as? String ?? "0"
//                  labelAnsPlus.text = arrSloats[2]["displayValue"] as? String ?? "+"
                    
                    fixCell.labelLoackTime.text = "Locked at:\(arrSelectedTicket[indexPath.row]["lockTime"] as? String ?? "--")"
                    //Selection
                    let arrSloatsCheck = arrSelectedTicket[indexPath.row]["slotes"] as! [[String: Any]]
                    let isSelectedMinus = arrSloatsCheck[0]["isSelected"] as? Bool ?? false
                    
                    if isSelectedMinus {
                        fixCell.buttonAnsMinus.backgroundColor = UIColor.white
                        fixCell.labelAnsMinus.textColor = UIColor.black
                    } else {
                        
                      //  fixCell.buttonAnsMinus.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
                        
                        fixCell.buttonAnsMinus.backgroundColor = UIColor.red
                        fixCell.labelAnsMinus.textColor = UIColor.white
                    }
                    
                    let isSelectedZero = arrSloatsCheck[1]["isSelected"] as? Bool ?? false
                    if isSelectedZero {
                        fixCell.buttonAnsZero.backgroundColor = UIColor.white
                        
                    } else {
                        fixCell.buttonAnsZero.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
                    }
                    
                    let isSelectedPlus = arrSloatsCheck[2]["isSelected"] as? Bool ?? false
                    if isSelectedPlus {
                        fixCell.buttonAnsPlus.backgroundColor = UIColor.white
                        fixCell.labelAnsPlus.textColor = UIColor.black
                    } else {
                       // fixCell.buttonAnsPlus.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
                        fixCell.buttonAnsPlus.backgroundColor = #colorLiteral(red: 0.01085097995, green: 0.3226040006, blue: 1, alpha: 1)
                        fixCell.labelAnsPlus.textColor = UIColor.white
                    }
                    
                    if isSelectedMinus {
                        fixCell.labelAnsLoacked.text = arrSloats[0]["displayValue"] as? String ?? "-"
                        
                    } else if isSelectedZero {
                        
                        fixCell.labelAnsLoacked.text = arrSloats[1]["displayValue"] as? String ?? "0"
                    } else if isSelectedPlus {
                        fixCell.labelAnsPlus.textColor = UIColor.black
                        fixCell.labelAnsLoacked.text = arrSloats[2]["displayValue"] as? String ?? "+"
                    } else {
                        fixCell.labelAnsLoacked.text = "Empty"
                    }
                    
                    return fixCell
                } else {
                    let fixCell = tableView.dequeueReusableCell(withIdentifier: "GameAnswerTwoTVC") as! GameAnswerTwoTVC
                    
                    let strAmount = "\(arrSelectedTicket[indexPath.row]["amount"]!)"
                    //fixCell.labelEntryFees.text = "₹\(MyModel().getNumbers(value: Double(strAmount)!))"
                    let test = Double(strAmount) ?? 0.00
                    fixCell.labelEntryFees.text = String(format: "₹ %.02f", test)
                    
                    
                    let strTickets = "\(arrSelectedTicket[indexPath.row]["totalTickets"]!)"
                    fixCell.labelTotalTickets.text = "\(MyModel().getNumbers(value: Double(strTickets)!))"
                    let strWinnings = "\(arrSelectedTicket[indexPath.row]["totalWinnings"]!)"
                    fixCell.labelTotalWinnig.text = "\(MyModel().getCurrncy(value: Double(strWinnings)!))"
                    let strWinners = "\(arrSelectedTicket[indexPath.row]["maxWinners"]!)"
                    fixCell.labelMaxWinners.text = "\(MyModel().getNumbers(value: Double(strWinners)!))"
                    
                    fixCell.labelAnsMinus.text = arrSloats[0]["displayValue"] as? String ?? "-"
                    fixCell.labelAnsZero.text = arrSloats[1]["displayValue"] as? String ?? "0"
                    fixCell.labelAnsPlus.text = arrSloats[2]["displayValue"] as? String ?? "+"
                    
                    
                    let string1 = arrSloats[0]["displayValue"] as? String ?? "-"
                    let string2 = arrSloats[2]["displayValue"] as? String ?? "-"
                                                      
                if string1.localizedCaseInsensitiveContains("red win")
                                                      {
                                                        
                        fixCell.vwWithradius.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.8862745098, blue: 0.8862745098, alpha: 1)
                                                         // fixCell.viewRange.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.8862745098, blue: 0.8862745098, alpha: 1)
                        fixCell.buttonAnsMinus.backgroundColor = UIColor.red
                        fixCell.labelAnsMinus.textColor = UIColor.black
                                                          
                                                      }
            if string2.localizedCaseInsensitiveContains("blue win")
            
            
            {
                                                    
            fixCell.buttonAnsPlus.backgroundColor = #colorLiteral(red: 0.01085097995, green: 0.3226040006, blue: 1, alpha: 1)
            fixCell.labelAnsPlus.textColor = UIColor.black
            
                
            }
                                       
//                    labelAnsMinus.text = arrSloats[0]["displayValue"] as? String ?? "-"
//                    labelAnsZero.text = arrSloats[1]["displayValue"] as? String ?? "0"
//                    labelAnsPlus.text = arrSloats[2]["displayValue"] as? String ?? "+"
                    
                    //Selection
                    let arrSloatsCheck = arrSelectedTicket[indexPath.row]["slotes"] as! [[String: Any]]
                    let isSelectedMinus = arrSloatsCheck[0]["isSelected"] as? Bool ?? false
                    
                    if isSelectedMinus {
                        fixCell.buttonAnsMinus.backgroundColor = UIColor.white
                    } else {
                       // fixCell.buttonAnsMinus.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
                        
                        fixCell.buttonAnsMinus.backgroundColor = UIColor.red
                        fixCell.labelAnsMinus.textColor = UIColor.white
                        
                    }
                    
                    
                    let isSelectedZero = arrSloatsCheck[1]["isSelected"] as? Bool ?? false
                    if isSelectedZero {
                        fixCell.buttonAnsZero.backgroundColor = UIColor.white
                    } else {
                        fixCell.buttonAnsZero.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
                    }
                    
                    let isSelectedPlus = arrSloatsCheck[2]["isSelected"] as? Bool ?? false
                    if isSelectedPlus {
                        fixCell.buttonAnsPlus.backgroundColor = UIColor.white
                        
                    } else {
                   //     fixCell.buttonAnsPlus.backgroundColor = #colorLiteral(red: 1, green: 0.7411764706, blue: 0.2549019608, alpha: 1)
                        
                        fixCell.buttonAnsPlus.backgroundColor = #colorLiteral(red: 0.01085097995, green: 0.3226040006, blue: 1, alpha: 1)
                        fixCell.labelAnsPlus.textColor = UIColor.white
                        
                    }
                    
                    if isSelectedMinus {
                        fixCell.labelAnsSelected.text = arrSloats[0]["displayValue"] as? String ?? "-"
                    } else if isSelectedZero {
                        fixCell.labelAnsSelected.text = arrSloats[1]["displayValue"] as? String ?? "0"
                    } else if isSelectedPlus {
                        fixCell.labelAnsSelected.text = arrSloats[2]["displayValue"] as? String ?? "+"
                    } else {
                        fixCell.labelAnsSelected.text = "Empty"
                    }
                    
                    if isGameStart {
                        fixCell.buttonAnsMinus.addTarget(self,
                                                         action: #selector(buttonAnswerOne(_:)),
                                                         for: .touchUpInside)
                        fixCell.buttonAnsMinus.tag = indexPath.row
                        fixCell.buttonAnsZero.addTarget(self,
                                                        action: #selector(buttonAnswerTwo(_:)),
                                                        for: .touchUpInside)
                        fixCell.buttonAnsZero.tag = indexPath.row
                        fixCell.buttonAnsPlus.addTarget(self,
                                                        action: #selector(buttonAnswerThree(_:)),
                                                        for: .touchUpInside)
                        fixCell.buttonAnsPlus.tag = indexPath.row
                        fixCell.buttonLockNow.isEnabled = true
                        fixCell.buttonLockNow.alpha = 1
                        btnlockall.isEnabled = true
                        btnlockall.alpha = 1
                        btnlockallnumber.isEnabled = true
                        btnlockallnumber.alpha = 1
                        fixCell.buttonLockNow.addTarget(self,
                                                        action: #selector(buttonLockNow(_:)),
                                                        for: .touchUpInside)
                        fixCell.buttonLockNow.tag = indexPath.row
                        
                        buttonAnsMinus.isEnabled = true
                        buttonAnsPlus.isEnabled = true
                        buttonAnsZero.isEnabled = true
                        
                         btnonee.isEnabled = true
                         btntwo.isEnabled = true
                         btnthree.isEnabled = true
                         btnfour.isEnabled = true
                         btnfive.isEnabled = true
                         btnsix.isEnabled = true
                         btnseven.isEnabled = true
                         btneeight.isEnabled = true
                         btnnine.isEnabled = true
                         btnzero.isEnabled = true
                        
                    } else {
                        
                        fixCell.buttonLockNow.isEnabled = false
                        fixCell.buttonLockNow.alpha = 0.5
                        btnlockall.isEnabled = false
                        btnlockall.alpha = 0.5
                        
                        btnlockallnumber.isEnabled = false
                        btnlockallnumber.alpha = 0.5
                        
                        buttonAnsMinus.isEnabled = false
                        buttonAnsPlus.isEnabled = false
                        buttonAnsZero.isEnabled = false
                        btnonee.isEnabled = false
                        btntwo.isEnabled = false
                        btnthree.isEnabled = false
                        btnfour.isEnabled = false
                        btnfive.isEnabled = false
                        btnsix.isEnabled = false
                        btnseven.isEnabled = false
                        btneeight.isEnabled = false
                        btnnine.isEnabled = false
                        btnzero.isEnabled = false
                    }
                    return fixCell
                }
            } else {
                
                let isLock = arrSelectedTicket[indexPath.row]["isLock"] as? Bool ?? false
                if isLock {
                    let fixCell = tableView.dequeueReusableCell(withIdentifier: "GameAnswerThreeLockTVC") as! GameAnswerThreeLockTVC
                    
                    let strAmount = "\(arrSelectedTicket[indexPath.row]["amount"]!)"
                    //fixCell.labelEntryFees.text = "₹\(MyModel().getNumbers(value: Double(strAmount)!))"
                    let test = Double(strAmount) ?? 0.00
                    fixCell.labelEntryFees.text = String(format: "₹ %.02f", test)
                    
                    
                    let strTickets = "\(arrSelectedTicket[indexPath.row]["totalTickets"]!)"
                    fixCell.labelTotalTickets.text = "\(MyModel().getNumbers(value: Double(strTickets)!))"
                    let strWinnings = "\(arrSelectedTicket[indexPath.row]["totalWinnings"]!)"
                    fixCell.labelTotalWinnig.text = "\(MyModel().getCurrncy(value: Double(strWinnings)!))"
                    let strWinners = "\(arrSelectedTicket[indexPath.row]["maxWinners"]!)"
                    fixCell.labelMaxWinners.text = "\(MyModel().getNumbers(value: Double(strWinners)!))"
                    
                    fixCell.widthView = view.frame.width - 105.0
                    
                    fixCell.arrData = arrSloats
                    
                    fixCell.labelLockTime.text = "Locked at:\(arrSelectedTicket[indexPath.row]["lockTime"] as? String ?? "--")"
                    
                    for item in arrSloats {
                        let isSelected = item["isSelected"] as? Bool ?? false
                        if isSelected {
                            fixCell.labelAnsSelected.text = item["displayValue"] as? String ?? "0"
                            
                        }
                    }
                    
                    return fixCell
                } else {
                   
                    let fixCell = tableView.dequeueReusableCell(withIdentifier: "GameAnswerThreeTVC",  for: indexPath) as! GameAnswerThreeTVC
                    
                    fixCell.delegate = self
                    let strAmount = "\(arrSelectedTicket[indexPath.row]["amount"]!)"
                    //fixCell.labelEntryFees.text = "₹\(MyModel().getNumbers(value: Double(strAmount)!))"
                    let test = Double(strAmount) ?? 0.00
                    fixCell.labelEntryFees.text = String(format: "₹ %.02f", test)
                    
                    
                    
                    let strTickets = "\(arrSelectedTicket[indexPath.row]["totalTickets"]!)"
                    fixCell.labelTotalTickets.text = "\(MyModel().getNumbers(value: Double(strTickets)!))"
                    let strWinnings = "\(arrSelectedTicket[indexPath.row]["totalWinnings"]!)"
                    fixCell.labelTotalWinnig.text = "\(MyModel().getCurrncy(value: Double(strWinnings)!))"
                    let strWinners = "\(arrSelectedTicket[indexPath.row]["maxWinners"]!)"
                    fixCell.labelMaxWinners.text = "\(MyModel().getNumbers(value: Double(strWinners)!))"
                    
                    fixCell.arrData = arrSloats
                    fixCell.isStart = isGameStart
                    
                    
                    
                    if isGameStart {
                        fixCell.buttonLockNow.isEnabled = true
                        fixCell.buttonLockNow.alpha = 1
                        fixCell.buttonLockNow.addTarget(self,
                                                        action: #selector(buttonLockNow(_:)),
                                                        for: .touchUpInside)
                        fixCell.buttonLockNow.tag = indexPath.row
                        
                        
                        btnlockall.isEnabled = true
                        btnlockall.alpha = 1
                        btnlockallnumber.isEnabled = true
                        btnlockallnumber.alpha = 1
                        btnonee.isEnabled = true
                        btntwo.isEnabled = true
                        btnthree.isEnabled = true
                        btnfour.isEnabled = true
                        btnfive.isEnabled = true
                        btnsix.isEnabled = true
                        btnseven.isEnabled = true
                        btneeight.isEnabled = true
                        btnnine.isEnabled = true
                        btnzero.isEnabled = true
                        
                    } else {
                        fixCell.buttonLockNow.isEnabled = false
                        fixCell.buttonLockNow.alpha = 0.5
                        
                        btnlockall.isEnabled = false
                        btnlockall.alpha = 0.5
                                      
                        btnlockallnumber.isEnabled = false
                        btnlockallnumber.alpha = 0.5
                                      
                                      buttonAnsMinus.isEnabled = false
                                      buttonAnsPlus.isEnabled = false
                                      buttonAnsZero.isEnabled = false
                                      btnonee.isEnabled = false
                                      btntwo.isEnabled = false
                                      btnthree.isEnabled = false
                                      btnfour.isEnabled = false
                                      btnfive.isEnabled = false
                                      btnsix.isEnabled = false
                                      btnseven.isEnabled = false
                                      btneeight.isEnabled = false
                                      btnnine.isEnabled = false
                                      btnzero.isEnabled = false
                    }
                    return fixCell
                }
            }
        }
    }
    
    //MARK: - TableView Button Method
    //Three Option Answer
    @objc func buttonAnswerOne(_ sender: UIButton) {
        let index = sender.tag
        var dictTicket = arrSelectedTicket[index]
        var arrSloats = dictTicket["slotes"] as! [[String: Any]]
        for (indexNo,_) in arrSloats.enumerated() {
            if indexNo == 0 {
                arrSloats[indexNo]["isSelected"] = NSNumber(value: true)
            } else {
                arrSloats[indexNo]["isSelected"] = NSNumber(value: false)
            }
        }
        print("Data: \(arrSloats)")
        dictTicket["slotes"] = arrSloats
        
        arrSelectedTicket[index] = dictTicket
        let indexPath = IndexPath(row: index, section: 0)
        tableAnswer.reloadRows(at: [indexPath], with: .none)
    }
    @objc func buttonAnswerTwo(_ sender: UIButton) {
        let index = sender.tag
        var dictTicket = arrSelectedTicket[index]
        var arrSloats = dictTicket["slotes"] as! [[String: Any]]
        for (indexNo,_) in arrSloats.enumerated() {
            if indexNo == 1 {
                arrSloats[indexNo]["isSelected"] = NSNumber(value: true)
            } else {
                arrSloats[indexNo]["isSelected"] = NSNumber(value: false)
            }
        }
        print("Data: \(arrSloats)")
        dictTicket["slotes"] = arrSloats
        
        arrSelectedTicket[index] = dictTicket
        let indexPath = IndexPath(row: index, section: 0)
        tableAnswer.reloadRows(at: [indexPath], with: .none)
    }
    @objc func buttonAnswerThree(_ sender: UIButton) {
        let index = sender.tag
        var dictTicket = arrSelectedTicket[index]
        var arrSloats = dictTicket["slotes"] as! [[String: Any]]
        for (indexNo,_) in arrSloats.enumerated() {
            if indexNo == 2 {
                arrSloats[indexNo]["isSelected"] = NSNumber(value: true)
            } else {
                arrSloats[indexNo]["isSelected"] = NSNumber(value: false)
            }
        }
        print("Data: \(arrSloats)")
        dictTicket["slotes"] = arrSloats
        
        arrSelectedTicket[index] = dictTicket
        let indexPath = IndexPath(row: index, section: 0)
        tableAnswer.reloadRows(at: [indexPath], with: .none)
    }
    
    @objc func buttonLockNow(_ sender: UIButton) {
        let index = sender.tag
        setAnswerLock(index: index)
    }
    
    func  setAnswerLock(index: Int) {
        var startValue: Int?
        var endValue: Int?
        var strDisplayValue: String?
        
        let arrSloatsSelect = arrSelectedTicket[index]["slotes"] as! [[String: Any]]
        
        for item in arrSloatsSelect {
            let isSelected = item["isSelected"] as? Bool ?? false
            if isSelected {
                
                startValue = item["startValue"] as? Int
                endValue = item["endValue"] as? Int
                strDisplayValue = item["displayValue"] as? String
                break
            }
        }
        
        if startValue == nil && endValue == nil {
            //Alert().showTost(message: "Select Answer", viewController: self)
        } else {
            let parameter:[String: Any] = ["userId": Define.USERDEFAULT.value(forKey: "UserID")!,
                                           "contestId": dictContest["id"]!,
                                           "contestPriceId": arrSelectedTicket[index]["contestPriceId"]!,
                                           "startValue": startValue!,
                                           "endValue": endValue!,
                                           "isLock": 1,
                                           "position": index,
                                           "displayValue": strDisplayValue!,
                                           ]
            print("Parameter: \(parameter)")
            
            let jsonString = MyModel().getJSONString(object: parameter)
            let encriptString = MyModel().encrypting(strData: jsonString!, strKey: Define.KEY)
            let strBase64 = encriptString.toBase64()
            
            SocketIOManager.sharedInstance.socket.emitWithAck("updateGame", strBase64!).timingOut(after: 0) { (data) in
                print("Data: \(data)")
                
                guard let strValue = data[0] as? String else {
                    Alert().showAlert(title: "Alert",
                                      message: Define.ERROR_SERVER,
                                      viewController: self)
                    return
                }
                
                let strJSON = MyModel().decrypting(strData: strValue, strKey: Define.KEY)
                let dictData = MyModel().convertToDictionary(text: strJSON)
                print("Get Data: \(dictData!)")
                
                let status = dictData!["statusCode"] as? Int ?? 0
                if status == 200 {
                    var dictItemData = dictData!["content"] as! [String: Any]
                  print(dictItemData)
                    var dictTicket = self.arrSelectedTicket[index]
                    dictTicket["isLock"] = dictItemData["isLock"]
                    dictTicket["lockTime"] = dictItemData["isLockTime"]
                    self.arrSelectedTicket[index] = dictTicket
                    let indexPath = IndexPath(row: index, section: 0)
                    self.tableAnswer.reloadRows(at: [indexPath], with: .none)
                }
            }
        }
        
    }
    
    @objc func buttonRangeAnsLock(_ sender: UIButton) {
        let index = sender.tag
        
        let indexPath = IndexPath(row: index, section: 0)
        let cell = tableAnswer.cellForRow(at: indexPath) as! GameAnsRangeTVC
        
        print(cell.rangeMin, cell.rangeMax)
        
        var startValue: Int?
        var endValue: Int?
        var strDisplayValue: String?
        
        startValue = Int(round(cell.rangeMin))
        endValue = Int(round(cell.rangeMax))
        strDisplayValue = "(\(startValue!) To \(endValue!))"
        
        if startValue == 0 && endValue == 0 {
            //Alert().showTost(message: "Select Answer range", viewController: self)
        } else {
            let parameter:[String: Any] = ["userId": Define.USERDEFAULT.value(forKey: "UserID")!,
                                           "contestId": dictContest["id"]!,
                                           "contestPriceId": arrSelectedTicket[index]["contestPriceId"]!,
                                           "startValue": startValue!,
                                           "endValue": endValue!,
                                           "isLock": 1,
                                           "position": index,
                                           "displayValue": strDisplayValue!,
                                           "lockAll" : ""
            ]
            print("Parameter: \(parameter)")
            
            let jsonString = MyModel().getJSONString(object: parameter)
            let encriptString = MyModel().encrypting(strData: jsonString!, strKey: Define.KEY)
            let strBase64 = encriptString.toBase64()
            cell.buttonLockNow.isEnabled = false
            SocketIOManager.sharedInstance.socket.emitWithAck("updateGame", strBase64!).timingOut(after: 0) { (data) in
                print("Data: \(data)")
                
                guard let strValue = data[0] as? String else {
                    Alert().showAlert(title: "Alert",
                                      message: Define.ERROR_SERVER,
                                      viewController: self)
                    return
                }
                
                
                let strJSON = MyModel().decrypting(strData: strValue, strKey: Define.KEY)
                let dictData = MyModel().convertToDictionary(text: strJSON)
                print("Get Data: \(dictData!)")
                
                let status = dictData!["statusCode"] as? Int ?? 0
                if status == 200 {
                    var dictItemData = dictData!["content"] as! [String: Any]
                    print(dictItemData)
                    var dictTicket = self.arrSelectedTicket[index]
                    print(dictTicket)
                    dictTicket["isLock"] = dictItemData["isLock"]
                    dictTicket["lockTime"] = dictItemData["isLockTime"]
                    dictTicket["startValue"] = dictItemData["startValue"]
                    dictTicket["endValue"] = dictItemData["endValue"]
                    dictTicket["displayValue"] = dictItemData["displayValue"]
                    let userSelectedData = ["startValue": dictItemData["startValue"]!,
                                            "endValue": dictItemData["endValue"]!]
                    dictTicket["user_select"] = userSelectedData
                    self.arrSelectedTicket[index] = dictTicket
                    //let indexPath = IndexPath(row: index, section: 0)
                    //self.tableAnswer.reloadRows(at: [indexPath], with: .none)
                    
                    cell.viewUnLocked.isHidden = true
                    cell.viewLocked.isHidden = false
                    cell.labelLockedAnswer.text = "\(dictItemData["startValue"]!) to \(dictItemData["endValue"]!)"
                    cell.labelLockedAt.text = "locked at: \(dictTicket["lockTime"] as? String ?? "--")"
                    cell.sliderAnswer.isUserInteractionEnabled = false
                }
            }
        }
    }
}

//MARK: - AnswerCell Delegate Method
extension GamePlayVC: GameAnswerThreeDelegate, GameAnsRangeDelegate {
    
    func getRanges(_ sender: GameAnsRangeTVC, minRange: Int, maxRange: Int, selectedRange: Int) {
        guard let index = tableAnswer.indexPath(for: sender) else {
            return
        }
        print("Index: \(index.row)")
        
        var dictData = arrSelectedTicket[index.row]
        dictData["selectedValue"] = Float(selectedRange)
        arrSelectedTicket[index.row] = dictData
        
        let indexPath = IndexPath(row: index.row, section: 0)
        let cell = tableAnswer.cellForRow(at: indexPath) as! GameAnsRangeTVC
        
        cell.labelAnsSelected.text = "\(minRange) to \(maxRange)"
    }
    
    
    func getSloatData(_ sender: GameAnswerThreeTVC, arrData: [[String : Any]]) {
        guard let index = tableAnswer.indexPath(for: sender) else {
            return
        }
        print("Index: \(index.row)")
        
        
        var dictTicket = arrSelectedTicket[index.row]
        
        dictTicket["slotes"] = arrData
        
        arrSelectedTicket[index.row] = dictTicket
        
        let indexPath = IndexPath(row: index.row, section: 0)
        let cell = tableAnswer.cellForRow(at: indexPath) as! GameAnswerThreeTVC
        
        for item in arrData {
            let isSelected = item["isSelected"] as? Bool ?? false
            if isSelected {
                
                cell.labelAnsSelected.text = item["displayValue"] as? String ?? "0"
            }
        }
    }
}

//MARK: - Alert Controller
extension GamePlayVC {
//    func contestError() {
//        let alertController = UIAlertController(title: "Contes", message: , preferredStyle: )
//
//    }
}
