//
//  T&CJTicketViewController.swift
//  CBit
//
//  Created by Mac on 13/03/20.
//  Copyright © 2020 Bhavik Kothari. All rights reserved.
//

import UIKit


class T_CJTicketViewController: UIViewController {

    @IBOutlet weak var webViewTermsAndCondition: UIWebView!
    @IBOutlet weak var progressTermsAndCondition: UIProgressView!
    
    override func viewDidLoad() {
       super.viewDidLoad()
        
                webViewTermsAndCondition.delegate = self
                webViewTermsAndCondition.loadRequest(URLRequest(url: URL(string: Define.JticketInfo_URl)!))
                
            }
            
            //MARK: - Button Method
            @IBAction func buttonMenu(_ sender: Any) {
                self.dismiss(animated: true, completion: nil)
            }
    @IBAction func btn_back(_ sender: Any) {
        
        self.dismiss(animated: true)
    }
}

        //MARK: - WebView Delegate Method
        extension T_CJTicketViewController: UIWebViewDelegate {
            func webViewDidStartLoad(_ webView: UIWebView) {
                progressTermsAndCondition.setProgress(0.1, animated: false)
            }
            func webViewDidFinishLoad(_ webView: UIWebView) {
                progressTermsAndCondition.setProgress(1.0, animated: true)
            }
            func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
                progressTermsAndCondition.setProgress(1.0, animated: true)
            }

            
            
}
