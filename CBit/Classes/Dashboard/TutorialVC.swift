import UIKit

class TutorialVC: UIViewController {
    //MARK: - Properties
    @IBOutlet weak var collectionViewTutorial: UICollectionView!
    @IBOutlet weak var buttonGotIt: ButtonWithBlueColor!
    @IBOutlet weak var buttonSkip: UIButton!
    
    var isFromeDashboard = Bool()
    
    var arrTutorialImages = [
        UIImage(named: "Step_1.png"),
        UIImage(named: "Step_2.png"),
        UIImage(named: "Step_3.png"),
        UIImage(named: "Step_4.png"),
        UIImage(named: "Step_5.png"),
        UIImage(named: "Step_6.png"),
        UIImage(named: "Step_7.png"),
        UIImage(named: "Step_8.png"),
        UIImage(named: "Step_9.png"),
        UIImage(named: "Step_10.png"),
        UIImage(named: "Step_11.png"),
        UIImage(named: "Step_12.png"),
        UIImage(named: "Step_13.png"),
        UIImage(named: "Step_14.png"),
        UIImage(named: "Step_15.png"),
        UIImage(named: "Step_16.png"),
        UIImage(named: "Step_17.png")
    ]
    
    var currentIndex = Int()
    
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonSkip.isHidden = false
    }
    
    //MARK: - Button Method
    @IBAction func buttonSkip(_ sender: UIButton) {
        if isFromeDashboard {
            self.dismiss(animated: true, completion: nil)
        } else {
            let storyBoard = UIStoryboard(name: "Dashboard", bundle: nil)
            let menuVC = storyBoard.instantiateViewController(withIdentifier: "MenuNC")
            menuVC.modalPresentationStyle = .fullScreen
            self.present(menuVC,
                         animated: true, completion:
                {
                    self.navigationController?.popToRootViewController(animated: true)
            })
        }
    }
    
    @IBAction func buttonGotIt(_ sender: UIButton) {
        if isFromeDashboard {
            self.dismiss(animated: true, completion: nil)
        } else {
            let storyBoard = UIStoryboard(name: "Dashboard", bundle: nil)
            let menuVC = storyBoard.instantiateViewController(withIdentifier: "MenuNC")
            menuVC.modalPresentationStyle = .fullScreen
            self.present(menuVC,
                         animated: true, completion:
                {
                    self.navigationController?.popToRootViewController(animated: true)
            })
        }
    }
}
//MARK: - Collection View Delegate Method
extension TutorialVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrTutorialImages.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionViewTutorial.frame.width, height: self.collectionViewTutorial.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewTutorial.dequeueReusableCell(withReuseIdentifier: "TutorialCVC", for: indexPath) as! TutorialCVC
        cell.imageTutorial.image = arrTutorialImages[indexPath.row]
        
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth = collectionViewTutorial.frame.width
        let currentPage = collectionViewTutorial.contentOffset.x / pageWidth
        
        if (0.0 != fmodf(Float(currentPage), 1.0))
        {
            currentIndex = Int(currentPage + 1);
        }
        else
        {
            currentIndex = Int(currentPage);
        }
        
        if currentIndex == (arrTutorialImages.count - 1) {
            self.buttonGotIt.isHidden = false
            self.buttonSkip.isHidden = true
        } else {
            self.buttonGotIt.isHidden = true
            self.buttonSkip.isHidden = false
        }
    }
}

//MARK: - Collection View Cell Class
class TutorialCVC: UICollectionViewCell {
    
    @IBOutlet weak var imageTutorial: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
