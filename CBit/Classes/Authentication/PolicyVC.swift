import UIKit

class PolicyVC: UIViewController {

    @IBOutlet weak var webViewTermsAndCondition: UIWebView!
    @IBOutlet weak var progressTermsAndCondition: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webViewTermsAndCondition.delegate = self
        webViewTermsAndCondition.loadRequest(URLRequest(url: URL(string: Define.LIGALITY_URL)!))
    }
    
    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}

//MARK: - WebView Delegate Method
extension PolicyVC: UIWebViewDelegate {
    func webViewDidStartLoad(_ webView: UIWebView) {
        progressTermsAndCondition.setProgress(0.1, animated: false)
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        progressTermsAndCondition.setProgress(1.0, animated: true)
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        progressTermsAndCondition.setProgress(1.0, animated: true)
    }
}
