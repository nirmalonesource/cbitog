import UIKit

class PrivacyPolicyVC: UIViewController {
    
    @IBOutlet weak var webViewPrivacyPolicy: UIWebView!
    @IBOutlet weak var progressPrivacyPilicy: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webViewPrivacyPolicy.delegate = self
        webViewPrivacyPolicy.loadRequest(URLRequest(url: URL(string: Define.PRIVACYPOLICY_URL)!))
    }
    
    
    
    //MARK: - Button Method
    @IBAction func buttonMenu(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: - WebView Delegate Method
extension PrivacyPolicyVC: UIWebViewDelegate {
    func webViewDidStartLoad(_ webView: UIWebView) {
        progressPrivacyPilicy.setProgress(0.1, animated: false)
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        progressPrivacyPilicy.setProgress(1.0, animated: true)
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        progressPrivacyPilicy.setProgress(1.0, animated: true)
    }
}
