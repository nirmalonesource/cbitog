//
//  SwiftPingPong.swift
//  CBit
//
//  Created by The FreeBird on 13/07/19.
//  Copyright © 2019 Bhavik Kothari. All rights reserved.
//


import UIKit

class SwiftPingPong: NSObject {
    
    static let shared = SwiftPingPong()
    var pingPongTimer: Timer?
    
    private var isSuccess = Bool()
    private var isFail = Bool()
    
    func startPingPong() {
        isSuccess = true
        isFail = false
        if pingPongTimer == nil {
            pingPongTimer = Timer.scheduledTimer(timeInterval: 4,
                                                 target: self,
                                                 selector: #selector(handlePingPongTimer(_:)),
                                                 userInfo: nil,
                                                 repeats: true)
        }
    }
    func stopPingPong() {
        if isFail {
            SocketIOManager.sharedInstance.establisConnection()
        }
        if pingPongTimer != nil {
            pingPongTimer!.invalidate()
            pingPongTimer = nil
        }
    }
    
    //TODO: HANDLE TIMER
    @objc func handlePingPongTimer(_ timer: Timer) {
        pingPongAPI()
    }
    
    //TODO: API
    private func pingPongAPI() {
        let url = URL(string: "\(Define.APP_URL)checkNetwork")!
        var request = URLRequest(url: url,
                                 cachePolicy: .reloadIgnoringCacheData,
                                 timeoutInterval: 3)
        request.setValue("iOS", forHTTPHeaderField: "User-Agent")
        request.setValue("close", forHTTPHeaderField: "Connection")
        
        let task = URLSession.shared.dataTask(with: request) { (data, urlSession, error) in
            if error == nil {
                print("Success")
                if !self.isSuccess {
                    self.isFail = false
                    self.isSuccess = true
                    print("----- Network Is Available -----")
                    DispatchQueue.main.sync {
                        SocketIOManager.sharedInstance.establisConnection()
                    }
                }
            } else {
                print("Failure")
                if !self.isFail {
                    self.isSuccess = false
                    self.isFail = true
                    print("----- Network Is Not Available -----")
                    
                    DispatchQueue.main.sync {
                        SocketIOManager.sharedInstance.closeConnection()
                        Define.APPDELEGATE.showLoading()
                    }
                }
            }
        }
        task.resume()
    }
}
